#!/usr/bin/env groovy

import com.example.Jar

def call(String branchName) {
    return new Jar(this).buildJarfile(branchName)
}
