#!/usr/bin/env groovy
package com.example

class Jar implements Serializable {

    def script

    Jar(script) {
        this.script = script
    }

    def buildJarfile(String branchName) {
        script.echo "building the application for $branchName"
        script.sh 'mvn package'
    }

}